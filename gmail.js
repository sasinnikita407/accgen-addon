/* 
* This file has the purpose of automating the gmail-forward setup for https://accgen.cathook.club. This is performed with user consent.
*/

var browser_api = chrome ? chrome : browser_api;

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function getEmailFromGmailPage() {
	var email = document.getElementById("guser").children[1].children[2].innerText;
	if (!email)
		return null;
	return email;
}

function changeStateNumber(number, filter_tries) {
	return new Promise(function (resolve, reject) {
		var obj = { setGmail: number, timeout: Date.now() + 5000, filter_tries: filter_tries };
		if (typeof number == "undefined")
			delete obj.setGmail;
		if (typeof filter_tries == "undefined")
			delete obj.filter_tries;
		browser_api.storage.local.set(obj, resolve);
	});
}

function sendMessage(message) {
	return new Promise(function (resolve, reject) {
		browser_api.runtime.sendMessage(message, resolve);
	});
}

browser_api.storage.local.get(['setGmail', 'timeout', 'filter_tries'], async function (result) {
	// Make sure we're not injecting JS into random gmail pages 5 months later
	if (Date.now() >= result.timeout || result.setGmail == false) {
		await changeStateNumber(false);
		return;
	}

	switch (result.setGmail) {
		// Forward already registered, but we still need to get the email (called by step 2)
		case -2:
			await changeStateNumber(-2);
			await sendMessage({ success: true, reason: "Already set up!", email: getEmailFromGmailPage() });
			// Chrome can't close its own tab at step 2 yet. The parent needs to do that.
			if (typeof browser != "undefined") window.close();
			break;
		// Go to page again after confirming html mode (called by step 1)
		case -1:
			await changeStateNumber(1);
			window.location = "https://mail.google.com/mail/?ui=html&zy=h&v=prufw";
			break;
		// Enter forward email
		case 1:
			// HTML email UI confirm detection
			if (document.querySelectorAll("a").length == 5) {
				await changeStateNumber(-1);
				document.querySelectorAll("input[type=submit]")[0].click();
				return;
			}
			document.querySelectorAll("input[type=text]")[0].value = "forward@mailhost.root.sx";
			await changeStateNumber(2);
			document.querySelectorAll("input[type=submit]")[0].click();
			break;
		// Confirm forward email and go to gmail filters page
		case 2:
			if (document.querySelectorAll("p[style='font-weight: bold; color: red;']")[0]) {
				await changeStateNumber(-2);
				window.location = "https://mail.google.com/mail/?ui=html&zy=h&v=prf";
				return;
			}

			// Prevent gmail from messing with the parent
			window.opener = null;
			await changeStateNumber(3);
			document.querySelectorAll("input[type=submit]")[0].click();
			break;
		// Go to gmail page (filters)
		case 3:
			// Chrome can now close its own tabs
			await changeStateNumber(4);
			window.location = "https://mail.google.com/mail/?ui=html&zy=h&v=prf";
			break;
		// Click add new filter
		case 4:
			await changeStateNumber(5);
			document.getElementsByName("nvp_bu_nftb")[0].click();
			break;
		// Add and set the conditions for the filter
		case 5:
			document.getElementsByName("cf1_from")[0].value = "noreply@steampowered.com";
			await changeStateNumber(6, 3);
			document.getElementsByName("nvp_bu_nxsb")[0].click()
			break;
		// Select forward address and create filter
		case 6:
			if (result.filter_tries <= 0) {
				await sendMessage({ success: false, reason: "Email forward wasn't confirmed by the email server." })
				window.close();
			}

			// Make sure our forward address has actually been confirmed.
			if (!document.getElementsByName('cf2_email')[0]) {
				await sleep(3000);
				// Firefox needs to go back a few pages, chrome can reload the current page
				await changeStateNumber(typeof browser == "undefined" ? 6 : 4, result.filter_tries - 1)
				if (typeof browser == "undefined")
					location.reload(true);
				else
					window.location = window.location;
				return;
			}

			document.getElementsByName('cf2_emc')[0].checked = true;

			var emails = document.getElementsByName('cf2_email')[0].options;
			var found = false
			for (var i in emails) {
				if (emails[i].innerText == "forward@mailhost.root.sx") {
					document.getElementsByName('cf2_email')[0].selectedIndex = i;
					found = true;
					break;
				}
			}
			if (!found) {
				await sleep(3000);
				// Firefox needs to go back a few pages, chrome can reload the current page
				await changeStateNumber(typeof browser == "undefined" ? 6 : 4, result.filter_tries - 1)
				if (typeof browser == "undefined")
					location.reload(true);
				else
					window.location = window.location;
				return;
			}

			await changeStateNumber(7);
			document.getElementsByName('nvp_bu_cftb')[0].click();
			break;
		// End
		case 7:
			// Also send email address to
			await sendMessage({ success: true, email: getEmailFromGmailPage() })
			await changeStateNumber(false);
			window.close();
			break;
		default:
			await changeStateNumber(false);
			window.close();
	}
});